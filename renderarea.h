#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QWidget>
#include <QColor>


class RenderArea : public QWidget
{
    Q_OBJECT
public:
    explicit RenderArea(QWidget *parent = nullptr);
    QSize minimumSizeHint() const Q_DECL_OVERRIDE;
    QSize sizeHint() const Q_DECL_OVERRIDE;

    enum ShapeType {Astroid, Cycloid};

    void setBackgroundColor (QColor color) {mBackgroundColor = color; }  //setter
    QColor getBackgroundColor () const {return mBackgroundColor; }  //getter

    void setShape (ShapeType shape) {mShape = shape; on_shape_changed();}
    ShapeType getShape() const {return mShape;}

    QPointF on_shape_changed();

    void setScale (double scale) {mScale = scale;}
    double getScale() const {return mScale;}

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;


signals:

public slots:

private:
    QColor mShapeColor;
    QColor mBackgroundColor;
    ShapeType mShape;

    int mStepCount;
    float mIntervalLength;
    float mScale;

private:
    QPointF compute (float t);
    QPointF computeAstroid(float t);
    QPointF computeCycloid(float t);

};

#endif // RENDERAREA_H
