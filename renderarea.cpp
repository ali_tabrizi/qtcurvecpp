#include "renderarea.h"
#include <QPaintEvent>
#include <QPainter>

RenderArea::RenderArea(QWidget *parent) :
    QWidget(parent),
    mShapeColor (255,255,255),
    mBackgroundColor (0,0,255),
    mShape (Astroid),
    mScale (40),
    mStepCount (64),
    mIntervalLength (2 * M_PI)
{

}


QSize RenderArea::minimumSizeHint() const
{
    return QSize(100, 100);
}
QSize RenderArea::sizeHint() const
{
    return QSize(400, 200);
}
QPointF RenderArea::on_shape_changed()
{
    switch (mShape) {
    case Astroid:
        mScale = 40;
        mIntervalLength = 2 * M_PI;
        mStepCount = 256;
        break;
    case Cycloid:
        mScale = 5;
        mIntervalLength = 10 * M_PI;
        mStepCount = 256;
        break;
    default:
        break;
    }
}

QPointF RenderArea::compute (float t)
{
    switch (mShape) {
    case Astroid:
        return computeAstroid(t);

    case Cycloid:
        return computeCycloid(t);

    default:
        break;
    }
    return QPointF(0, 0);
}

QPointF RenderArea::computeAstroid(float t)
{
    float sint = sin(t);
    float cost = cos(t);

    float x = 2 * cost * cost * cost;
    float y = 2 * sint * sint * sint;

    return QPointF(x, y);
}

QPointF RenderArea::computeCycloid(float t)
{
    return QPointF(
                1.5 * (1 - cos(t)), // X
                1.5 * (t - sin(t))  //Y
                );
}

void RenderArea::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setBrush(QColor(mBackgroundColor));
    painter.setPen(mShapeColor);
    painter.drawRect(this->rect());

    QPoint center = this->rect().center();
    float step = mIntervalLength / mStepCount;
    QPointF prevPoint = compute(0);
    QPoint prevPixel;
    prevPixel.setX(prevPoint.x() * mScale + center.x());
    prevPixel.setY(prevPoint.y() * mScale + center.y());

    for (float t = step; t < mIntervalLength; t += step) {
        QPointF point = compute(t);

        QPoint pixel;
        pixel.setX(point.x() * mScale + center.x());
        pixel.setY(point.y() * mScale + center.y());

        painter.drawLine(pixel, prevPixel);
        prevPixel = pixel;
    }


}


